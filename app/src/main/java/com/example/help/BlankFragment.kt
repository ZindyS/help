package com.example.help

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import androidx.browser.customtabs.CustomTabsClient.getPackageName
import androidx.fragment.app.Fragment


class BlankFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_blank2, container, false)

        val cm: ConnectivityManager =
            context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork: NetworkInfo = cm.getActiveNetworkInfo()
        val isConnected = activeNetwork.isConnectedOrConnecting

        if(isConnected) {
            val videoView = root.findViewById<VideoView>(R.id.videoView)

            val myVideoUri =
                Uri.parse("android.resource://com.example.help/" + R.raw.yeet)
            videoView.setVideoURI(myVideoUri)

            videoView.setMediaController(MediaController(context))
            videoView.requestFocus(0)
            videoView.start()
        } else {
            Toast.makeText(context, "Проверьте подключение", Toast.LENGTH_SHORT).show()
        }

        return root
    }
}