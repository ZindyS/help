package com.example.help

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.ads.*


class BlankFragment1 : Fragment() {

    private lateinit var mInterstitialAd: InterstitialAd

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MobileAds.initialize(context,
            "ca-app-pub-2520166005239259/9122502970")

        mInterstitialAd = InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        mInterstitialAd.loadAd(AdRequest.Builder().build())


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_blank, container, false)
        MobileAds.initialize(context) {}

        val button = root.findViewById<Button>(R.id.button)
        val mAdView = root.findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        mAdView.visibility = View.GONE

        mAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mAdView.visibility = View.VISIBLE
            }
        }

        button.setOnClickListener {
            if (mInterstitialAd.isLoaded) {
                mInterstitialAd.show()
            }
            val builder: NotificationCompat.Builder = NotificationCompat.Builder(context!!, "SOME_CHANNEL")
                .setSmallIcon(R.drawable.someicon)
                .setContentTitle("Оповещение")
                .setContentText("Арура")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            val notificationManager =
                NotificationManagerCompat.from(context!!)
            notificationManager.notify(101, builder.build())
        }
        return root
    }
}