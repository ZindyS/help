package com.example.help

data class User (
    var password: String = "",
    var height: Long = 0,
    var weight: Long = 0
)