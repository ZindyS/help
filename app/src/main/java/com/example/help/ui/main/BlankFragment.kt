package com.example.help.ui.main

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.help.R
import com.example.help.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlin.concurrent.timerTask

class BlankFragment : Fragment() {

    lateinit var root: View
    var auth = FirebaseAuth.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_login, container, false)

        val username = root.findViewById<EditText>(R.id.username)
        val password = root.findViewById<EditText>(R.id.password)
        val login = root.findViewById<Button>(R.id.login)
        val signup = root.findViewById<Button>(R.id.signup)


        login.setOnClickListener {
            if(!TextUtils.isEmpty(username.text) && !TextUtils.isEmpty(password.text)) {
                auth.signInAnonymously()
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            val cur = auth.currentUser
                            updateUI(cur, username.text.toString(), password.text.toString())
                        } else {
                            Toast.makeText(context, "Error404", Toast.LENGTH_SHORT).show()
                        }
                    }
            } else {
                Toast.makeText(context, "Проверьте поля", Toast.LENGTH_SHORT).show()
            }
        }

        signup.setOnClickListener {
            if(!TextUtils.isEmpty(username.text) && !TextUtils.isEmpty(password.text)) {
                auth.signInAnonymously()
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            val cur = auth.currentUser
                            signUpUI(cur, username.text.toString(), password.text.toString())
                        } else {
                            Toast.makeText(context, "Error404", Toast.LENGTH_SHORT).show()
                        }
                    }
            } else {
                Toast.makeText(context, "Проверьте поля", Toast.LENGTH_SHORT).show()
            }
        }

        return root
    }

    private fun signUpUI(cur: FirebaseUser?, usertext: String, passtext: String) {
        if (cur != null) {
            Firebase.database.reference.child("users").child(usertext).child("password").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()) {
                        Toast.makeText(context, "Пользователь с таким именем уже существует", Toast.LENGTH_SHORT).show()
                    } else {
                        val user = User(passtext, 1, 1)
                        Firebase.database.reference.child("users").child(usertext).setValue(user)
                        Toast.makeText(context, "yeet", Toast.LENGTH_SHORT).show()
                    }
                }

            })
        }
    }

    private fun updateUI(cur: FirebaseUser?, usertext: String, passtext: String) {
        if (cur != null) {
            Firebase.database.reference.child("users").child(usertext).child("password").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {}

                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()) {
                        if (snapshot.value.toString().equals(passtext)) {
                            Toast.makeText(context, "Voshel", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(context, "Проверьте поля", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, "Проверьте поля", Toast.LENGTH_SHORT).show()
                    }
                }

            })
        } else {
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
        }
    }
}